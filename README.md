# APIs

Activities with APIs.

## Setup

Prior to all activities, install all the required dependencies within a Python
virtual environment using `pipenv`:

```bash
pipenv install
```

## Activities

All activites are Jupyter Notebooks. To run them, launch the Jupyter Notebook interface:

```bash
jupyter notebook
```
